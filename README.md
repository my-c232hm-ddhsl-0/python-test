# PythonTest

## 概要
C232HM-DDHSL-0ケーブルをPythonで制御するプロジェクトです。  
メーカーサイトは[こちら](https://ftdichip.com/products/c232hm-ddhsl-0-2/)です。

## 開発環境
Pythonおよび使用ライブラリのバージョンは以下の通りです。  
- Python 3.10.11
- Pyftdi 0.55.0
- Pyusb 1.2.1

なお、本リポジトリのPythonスクリプトを動作させるには、ケーブルのUSBドライバを差し替える必要があります。  
Zadigを使用し、USBドライバをlibusbKに差し替えます。

## 通信制御デバイス
- LED/スイッチ
- 秋月電子 BME280使用 温湿度・気圧センサモジュールキット（[リンク](https://akizukidenshi.com/catalog/g/gK-09421/)）
- sparkfun ICM-20948搭載 9DoF IMUモジュール（[リンク](https://www.switch-science.com/products/5854)）
- スイッチサイエンス 照度・近接一体型センサモジュール RPR-0521RS搭載（[リンク](https://www.switch-science.com/products/2770)）
