from pyftdi.gpio import GpioAsyncController

class DevGpio:
    """GPIO制御
    """

    def __init__(self, direction=0x00):
        """コンストラクタ

        Args:
            direction (hexadecimal, optional): 入出力設定（全ピン一括）
        """
        self.__gpio = GpioAsyncController()
        self.__gpio.configure("ftdi:///1", direction=direction)

    def __del__(self):
        """デストラクタ
        """
        self.__gpio.close()

    def set_dir(self, pin, is_out):
        """入出力変更

        Args:
            pin (int): ピン番号
            is_out (bool): True:Output、False:Input
        """
        if pin < 8:
            port = 0x01 << pin
            dir = self.__gpio.direction
            if is_out:
                dir |= port
            else:
                dir &= ~port
            self.__gpio.set_direction(port, dir)
    
    def write_pin(self, pin, is_high):
        """ピン出力変更

        Args:
            pin (int): ピン番号
            is_high (bool): True:High、False:Low
        """
        if pin < 8:
            port = 0x01 << pin
            val = self.__gpio.read()
            if is_high:
                val |= port
            else:
                val &= ~port
            self.__gpio.write(val & self.__gpio.direction)

    def write_pin_logic(self, pin, is_on, logic=True):
        """ピン出力変更（論理変換付き）

        Args:
            pin (int): ピン番号
            is_on (bool): True:ON、False:OFF
            logic (bool, optional): True:High Active、False:Low Active
        """
        if not logic:
            is_on = not is_on
        self.write_pin(pin, is_on)

    def read_pin(self, pin):
        """ピン入力読み出し

        Args:
            pin (int): ピン番号

        Returns:
            bool: True:High、False:Low
        """
        if pin < 8:
            val = self.__gpio.read() & (0x01 << pin)
        else:
            val = 0
        return val != 0

    def read_pin_logic(self, pin, logic=True):
        """ピン入力読み出し（論理変換）

        Args:
            pin (int): ピン番号
            logic (bool, optional): True:High Active、False:Low Active

        Returns:
            bool: True:ON、False:OFF
        """
        is_on = self.read_pin(pin)
        if not logic:
            is_on = not is_on
        return is_on
