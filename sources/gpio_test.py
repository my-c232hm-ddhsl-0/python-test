"""
GPIO制御テスト

"""

import time
from dev_gpio import DevGpio

PIN_LED = 4
PIN_SW = 5

gpio = DevGpio()
gpio.set_dir(PIN_LED, True)
gpio.set_dir(PIN_SW, False)

try:
    while True:
        gpio.write_pin_logic(PIN_LED, gpio.read_pin_logic(PIN_SW, False))
        time.sleep(0.01)
except KeyboardInterrupt:
    pass

gpio.close()
