"""
BME280 ID取得テスト

"""
from pyftdi.spi import SpiController
from time import sleep

REG_ADDR_ID = 0xd0

spi = SpiController()
spi.configure('ftdi://::/1')
slave = spi.get_port(cs=0, freq=1E6, mode=0)

try:
    while True:
        rdata = slave.exchange([REG_ADDR_ID], 1)
        rdata_list = bytearray(rdata)
        print(hex(rdata_list[0]))
        sleep(3)
except KeyboardInterrupt:
    pass

spi.close()