"""
ICM-20948 磁気センサ動作テスト

"""
from icm20948 import Icm20948
from time import sleep

imu = Icm20948(0, 4E6)
imu.soft_reset()
imu.sleep_mode(False)

imu.i2c_mst_enable(True)
if not imu.check_id_mag():
    print("ID read error.")
    exit()

imu.get_mag_enable(Icm20948.MAG_MODE_CONT_10HZ)
print("Get raw value from magnetometer.")
try:
    while True:
        mag_x, mag_y, mag_z = imu.get_raw_mag()
        print(f"Raw: X = {mag_x:6d}, Y = {mag_y:6d}, Z = {mag_z:6d}")
        sleep(1)
except KeyboardInterrupt:
    pass
imu.get_mag_disable()
