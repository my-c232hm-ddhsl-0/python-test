"""
BME280制御テストプログラム

"""
from time import sleep
from bme280 import Bme280

CS_CH = 0
SPI_SCK_FREQ = 1E6

th_sens = Bme280(CS_CH, SPI_SCK_FREQ)

# 単一スキャン
print("Single Scan Start")
th_sens.setup(1)
while not th_sens.is_measuring():               # 計測開始待ち
    sleep(0.001)
while th_sens.is_measuring():                   # 計測終了待ち
    sleep(0.001)
th_sens.read_data(True)                         # 計測値読み出し

# 連続スキャン
print("Continuous Scan Start")
th_sens.setup(3)
try:
    while True:
        sleep(1)
        th_sens.read_data(True)

except KeyboardInterrupt:
    pass
