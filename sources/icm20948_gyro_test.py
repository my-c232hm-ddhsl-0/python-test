"""
ICM-20948 ジャイロセンサ動作テスト（Y軸回転）

Sample mode: Default setting(Continuous)
Full scale range: Default setting(250dps)
"""
from icm20948 import Icm20948
from time import sleep

SENSING_INTERVAL_SEC = 0.2
PRINT_DEG_INTERVAL_SEC = 0.2

GYRO_SAMPLE_RATE_MS = 2
GYRO_SAMPLE_RATE_HZ = 1000 / GYRO_SAMPLE_RATE_MS
GYRO_LPF_VAL = Icm20948.GYRO_DLPFCFG_FC_196

EXEC_CALIB_GYRO = True
CALIB_GYRO_SAMPLE_COUNT = 10000

imu = Icm20948(0, 4E6)
if not imu.check_id():
    print("ID read error.")
    exit()
imu.soft_reset()
imu.sleep_mode(False)

imu.set_sample_rate_gyro(GYRO_SAMPLE_RATE_HZ)
imu.set_lpf_gyro(True, GYRO_LPF_VAL)

if EXEC_CALIB_GYRO:
    print("Starting gyro calibration.")
    calib_x, calib_y, calib_z = imu.exec_calib_gyro(CALIB_GYRO_SAMPLE_COUNT)
    print(f"Gyro Calibration: X = {calib_x:.2f}, Y = {calib_y:.2f}, Z = {calib_z:.2f}")

imu.reset_fifo()
fifo_enable_sens = Icm20948.FIFO_EN_GYRO_X | Icm20948.FIFO_EN_GYRO_Y | Icm20948.FIFO_EN_GYRO_Z
imu.set_fifo_sens(fifo_enable_sens, True)

print("Get raw value from gyro.")
degree_y = 0.0
sens_time = 0
try:
    while True:
        fifo_count = imu.get_fifo_count()
        accl_raw, gyro_x_raw, gyro_y_raw, gyro_z_raw = imu.decode_fifo_data(imu.read_fifo(fifo_count), fifo_enable_sens)
        degree_y += imu.calc_gyro_degree(gyro_y_raw)
        if sens_time > PRINT_DEG_INTERVAL_SEC:
            print(f"{degree_y:.2f}[deg]")
            sens_time = 0
        sleep(SENSING_INTERVAL_SEC)
        sens_time += SENSING_INTERVAL_SEC
except KeyboardInterrupt:
    pass
