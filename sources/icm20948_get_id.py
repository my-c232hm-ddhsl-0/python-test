"""
ICM-20948 ID取得テスト

"""
from pyftdi.spi import SpiController
from time import sleep
import itertools

spi = SpiController()
spi.configure('ftdi://::/1')
slave = spi.get_port(cs=0, freq=1E6, mode=0)

REG_BANK_SEL = 0x7F
AGB0_REG_WHO_AM_I = 0x00
VALIED_CHIP_ID = 0xea

def spi_xfer(xfer_list, read_len=0):
    rdata = slave.exchange(xfer_list, read_len)
    return rdata

def write_reg(reg_addr, val_list):
    w_data = [[reg_addr], val_list]
    w_data_list = list(itertools.chain.from_iterable(w_data))
    spi_xfer(w_data_list)

def read_reg(reg_addr, read_len):
    rdata = spi_xfer([reg_addr | 0x80], read_len)
    rdata_list = bytearray(rdata)
    return rdata_list

def set_bank(bank):
    if bank > 3:
        return False
    bank = ((bank << 4) & 0x30)
    write_reg(REG_BANK_SEL, [bank])

try:
    while True:
        set_bank(0)
        rdata = read_reg(AGB0_REG_WHO_AM_I, 1)
        print(f"Read Chip ID = {hex(rdata[0])} ({hex(VALIED_CHIP_ID)})")
        sleep(3)
except KeyboardInterrupt:
    pass

spi.close()
