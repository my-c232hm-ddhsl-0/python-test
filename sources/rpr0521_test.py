"""
RPR-0521RS制御テストプログラム

"""
from rpr0521 import Rpr0521
from time import sleep

light = Rpr0521(400E3)
if not light.check_id():
    print("ID read error.")
    exit()
light.reset()
light.setup(True, Rpr0521.MODE_MESU_ALS_400_PS_OFF)
sleep(0.5)

try:
    while True:
        als0, als1 = light.get_als_raw()
        print(f"ALS0 = {als0} ALS1 = {als1}")
        sleep(1)
except KeyboardInterrupt:
    pass
