"""
ICM-20948 磁気センサID取得テスト

"""
from icm20948 import Icm20948
from time import sleep

VALIED_MAG_ID = 0x09
MAG_I2C_SLAVE_ADDR = 0x0c
MAG_REG_WIA2 = 0x01
AGB0_REG_INT_PIN_CONFIG = 0x0F
AGB0_REG_EXT_SLV_SENS_DATA_00 = 0x3B
AGB3_REG_I2C_MST_CTRL = 0x01
AGB3_REG_I2C_SLV0_ADDR = 0x03
AGB3_REG_I2C_SLV0_REG = 0x04
AGB3_REG_I2C_SLV0_CTRL = 0x05

def i2c_mst_pass_through(imu, is_en):
    imu.set_bank(0)
    imu.write_reg_1bit(AGB0_REG_INT_PIN_CONFIG, 1, is_en)

def i2c_mst_enable(imu, is_en):
    i2c_mst_pass_through(imu, False)
    imu.set_bank(3)
    # 345.6kHz/Duty 46.67%(Recommended), I2C stop between slave reads
    imu.write_reg(AGB3_REG_I2C_MST_CTRL, [0b00010111])
    imu.set_bank(0)
    imu.write_reg_1bit(Icm20948.AGB0_REG_USER_CTRL, 5, is_en)

def i2c_mst_slv_en(imu, ch, slv_addr, reg, is_read, read_len):
    if ch > 3:
        return
    slv_addr_reg = AGB3_REG_I2C_SLV0_ADDR + 4 * ch
    slv_reg_reg = AGB3_REG_I2C_SLV0_REG + 4 * ch
    slv_ctrl_reg = AGB3_REG_I2C_SLV0_CTRL + 4 * ch
    imu.set_bank(3)
    if is_read:
        if read_len > 15:
            return
        slv_addr |= 0x80
    imu.write_reg(slv_addr_reg, [slv_addr])
    imu.write_reg(slv_reg_reg, [reg])
    imu.write_reg(slv_ctrl_reg, [0b10000000 | read_len])

imu = Icm20948(0, 4E6)
imu.soft_reset()
imu.sleep_mode(False)

i2c_mst_enable(imu, True)
i2c_mst_slv_en(imu, 0, MAG_I2C_SLAVE_ADDR, MAG_REG_WIA2, True, 1)
imu.set_bank(0)
read_time = 0
while read_time < 0.5:
    sleep(0.01)
    id = imu.read_reg_1(AGB0_REG_EXT_SLV_SENS_DATA_00)
    if id == VALIED_MAG_ID:
        break
    read_time += 0.01
if id >= 0:
    print(f"Magnetometer ID = {hex(id)}({hex(VALIED_MAG_ID)})")
else:
    print("Read ID error.")
