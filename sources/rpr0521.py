from pyftdi.i2c import I2cController

class Rpr0521:
    """RPR-0521制御モジュール

    RPR-0521RSを制御する。
    """

    SLAVE_ADDR                  = 0x38

    REG_SYSTEM_CTRL             = 0x40
    REG_MODE_CTRL               = 0x41
    REG_DATA0_LSB               = 0x46
    REG_MANUFACT_ID             = 0x92

    VALIED_CHIP_ID              = 0xe0

    MODE_MESU_ALS_400_PS_OFF    = 10

    def __init__(self, freq_hz=100E3):
        """RPR-0521制御モジュール初期化

        Args:
            freq_hz (float): I2Cクロック周波数[Hz]
        """
        self.i2c = I2cController()
        self.i2c.configure('ftdi://::/1', frequency=freq_hz)
        self.slave = self.i2c.get_port(self.SLAVE_ADDR)

    def __del__(self):
        """デストラクタ
        """
        self.i2c.close()

    def write_reg_1(self, addr, val):
        """1レジスタ書き込み

        Args:
            addr (int): レジスタアドレス
            val (int): 書き込み値
        """
        self.slave.write_to(addr, val.to_bytes(1, "little"))

    def read_reg(self, addr, len):
        """レジスタ読み出し

        Args:
            addr (int): レジスタアドレス
            len (int): 読み出し長さ（バイト数）

        Returns:
            list: 読み出し値
        """
        rdata = self.slave.exchange([addr], len)
        rdata_list = bytearray(rdata)
        return rdata_list
    
    def read_reg_1(self, addr):
        """1レジスタ読み出し

        Args:
            addr (int): レジスタアドレス

        Returns:
            int: 読み出し値
        """
        return self.read_reg(addr, 1)[0]

    def reset(self):
        """リセット実行
        """
        self.write_reg_1(self.REG_SYSTEM_CTRL, 0x80)

    def check_id(self, is_disp=False):
        """ID読み出し

        Args:
            is_disp (bool): 読み出したIDを標準出力するか否か

        Returns:
            bool: IDを正しく読み出せたか否か
        """
        id_val = self.read_reg_1(self.REG_MANUFACT_ID)
        if is_disp:
            print(f"ID = 0x{id_val:x}")
        return (id_val == self.VALIED_CHIP_ID)

    def setup(self, is_als_en, measure):
        """制御セットアップ

        Args:
            is_als_en (bool): ALS測定オン/オフ
            measure (int): 測定時間選択（MODE_MESU_ALS_***_PS_***を設定する。）
        """
        reg = 0x80 if is_als_en else 0x00
        reg |= measure
        self.write_reg_1(self.REG_MODE_CTRL, reg)

    def get_als_raw(self):
        """ALSデータ取得

        Returns:
            int, int: ALS0データ、ALS1データ
        """
        rdata = self.read_reg(self.REG_DATA0_LSB, 4)
        als0 = ((rdata[1] << 8) | (rdata[0] & 0xff))
        als1 = ((rdata[3] << 8) | (rdata[2] & 0xff))
        return als0, als1
