"""
RPR-0521RS ID取得テスト

"""
from pyftdi.i2c import I2cController
from time import sleep


SLAVE_ADDR = 0x38
REG_ADDR_ID = 0x92
VALIED_CHIP_ID = 0xe0

i2c = I2cController()
i2c.configure('ftdi://::/1')
slave = i2c.get_port(SLAVE_ADDR)

try:
    while True:
        rdata = slave.exchange([REG_ADDR_ID], 1)
        rdata_list = bytearray(rdata)
        print(f"Read Chip ID = {hex(rdata_list[0])} ({hex(VALIED_CHIP_ID)})")
        sleep(3)
except KeyboardInterrupt:
    pass

i2c.close()
