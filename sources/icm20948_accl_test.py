"""
ICM-20948 加速度センサ動作テスト

"""
from icm20948 import Icm20948
from time import sleep

SENSING_INTERVAL_SEC = 0.02
PRINT_DEG_INTERVAL_SEC = 0.2

ACCL_SAMPLE_RATE_MS = 1
ACCL_SAMPLE_RATE_HZ = 1000 / ACCL_SAMPLE_RATE_MS
ACCL_FSR = Icm20948.ACCL_FSR_4G
ACCL_LPF_VAL = Icm20948.ACCL_DLPFCFG_FC_246

EXEC_CALIB_ACCL = True
CALIB_ACCL_SAMPLE_COUNT = 10000

imu = Icm20948(0, 4E6)
if not imu.check_id():
    print("ID read error.")
    exit()
imu.soft_reset()
imu.sleep_mode(False)

imu.set_fsr_accl(ACCL_FSR)
imu.set_sample_rate_accl(ACCL_SAMPLE_RATE_HZ)
imu.set_lpf_accl(True, ACCL_LPF_VAL)

if EXEC_CALIB_ACCL:
    print("Starting accel calibration.")
    calib_x, calib_y, calib_z = imu.exec_calib_accl(CALIB_ACCL_SAMPLE_COUNT)
    print(f"Accel Calibration: X = {calib_x:.2f}, Y = {calib_y:.2f}, Z = {calib_z:.2f}")

imu.reset_fifo()
fifo_enable_sens = Icm20948.FIFO_EN_ACCL
imu.set_fifo_sens(fifo_enable_sens, True)

print("Get raw value from accel.")
sens_time = 0
try:
    while True:
        sleep(SENSING_INTERVAL_SEC)
        sens_time += SENSING_INTERVAL_SEC
        fifo_count = imu.get_fifo_count()
        if fifo_count == 4096:
            print("FIFO Overflow!")
        accl_raw, _, _, _ = imu.decode_fifo_data(imu.read_fifo(fifo_count), fifo_enable_sens)
        accl_raw_y = imu.get_fifo_accl_axis(accl_raw, Icm20948.AXIS_Y)
        velocity = imu.calc_accl_velocity(accl_raw_y)
        if sens_time > PRINT_DEG_INTERVAL_SEC:
            print(f"{velocity * 1000:.2f}[mm/s]")
            sens_time = 0
except KeyboardInterrupt:
    pass
