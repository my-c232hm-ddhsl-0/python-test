from pyftdi.spi import SpiController
import itertools
import time
import numpy as np

class Icm20948:
    """ICM-20948制御モジュール

    ICM-20948を制御する。
    通信方式はSPI 4線式とする。SPIモードは0とする。
    """

    REG_BANK_SEL                    = 0x7F
    
    AGB0_REG_WHO_AM_I               = 0x00
    AGB0_REG_USER_CTRL              = 0x03
    AGB0_REG_PWR_MGMT_1             = 0x06
    AGB0_REG_INT_PIN_CONFIG         = 0x0F
    AGB0_REG_INT_ENABLE             = 0x10
    AGB0_REG_INT_ENABLE_1           = 0x11
    AGB0_REG_INT_ENABLE_2           = 0x12
    AGB0_REG_INT_ENABLE_3           = 0x13
    AGB0_REG_I2C_MST_STATUS         = 0x17
    AGB0_REG_INT_STATUS_2           = 0x1b
    AGB0_REG_INT_STATUS_3           = 0x1c
    AGB0_REG_ACCEL_XOUT_H           = 0x2D
    AGB0_REG_GYRO_XOUT_H            = 0x33
    AGB0_REG_EXT_SLV_SENS_DATA_00   = 0x3B
    AGB0_REG_FIFO_EN_1              = 0x66 
    AGB0_REG_FIFO_EN_2              = 0x67
    AGB0_REG_FIFO_RST               = 0x68
    AGB0_REG_FIFO_COUNT_H           = 0x70
    AGB0_REG_FIFO_R_W               = 0x72

    AGB2_REG_GYRO_SMPLRT_DIV        = 0x00
    AGB2_REG_GYRO_CONFIG_1          = 0x01
    AGB2_REG_ACCEL_SMPLRT_DIV_1     = 0x10
    AGB2_REG_ACCEL_CONFIG_1         = 0x14

    AGB3_REG_I2C_MST_CTRL           = 0x01
    AGB3_REG_I2C_SLV0_ADDR          = 0x03
    AGB3_REG_I2C_SLV0_REG           = 0x04
    AGB3_REG_I2C_SLV0_CTRL          = 0x05
    AGB3_REG_I2C_SLV0_DO            = 0x06
    AGB3_REG_I2C_SLV4_ADDR          = 0x13
    AGB3_REG_I2C_SLV4_REG           = 0x14
    AGB3_REG_I2C_SLV4_CTRL          = 0x15
    AGB3_REG_I2C_SLV4_DO            = 0x16
    AGB3_REG_I2C_SLV4_DI            = 0x17

    I2C_MAG_SLV_CH                  = 0
    
    VALIED_CHIP_ID                  = 0xea

    AXIS_X                          = 0
    AXIS_Y                          = 1
    AXIS_Z                          = 2

    GYRO_SMPLRT_HZ_MAX              = 1125
    GYRO_SMPLRT_HZ_MIN              = 4.3
    GYRO_SMPLRT_HZ_DEF              = 1125
    GYRO_DLPFCFG_FC_196             = 0x00
    GYRO_DLPFCFG_FC_151             = 0x01
    GYRO_DLPFCFG_FC_119             = 0x02
    GYRO_DLPFCFG_FC_51              = 0x03
    GYRO_DLPFCFG_FC_23              = 0x04
    GYRO_DLPFCFG_FC_11              = 0x05
    GYRO_DLPFCFG_FC_5               = 0x06
    GYRO_DLPFCFG_FC_361             = 0x07

    ACCL_SMPLRT_HZ_MAX              = 1125
    ACCL_SMPLRT_HZ_MIN              = 0.28
    ACCL_SMPLRT_HZ_DEF              = 1125
    ACCL_DLPFCFG_FC_246             = 0x00
    ACCL_DLPFCFG_FC_111             = 0x02
    ACCL_DLPFCFG_FC_50              = 0x03
    ACCL_DLPFCFG_FC_23              = 0x04
    ACCL_DLPFCFG_FC_11              = 0x05
    ACCL_DLPFCFG_FC_5               = 0x06
    ACCL_DLPFCFG_FC_473             = 0x07
    ACCL_FSR_2G                     = 0x00
    ACCL_FSR_4G                     = 0x01
    ACCL_FSR_8G                     = 0x02
    ACCL_FSR_16G                    = 0x03
    ACCL_FSR_MAX                    = ACCL_FSR_16G
    ACCL_FSR_DEF                    = ACCL_FSR_2G

    FIFO_EN_ACCL                    = 0x10
    FIFO_EN_GYRO_Z                  = 0x08
    FIFO_EN_GYRO_Y                  = 0x04
    FIFO_EN_GYRO_X                  = 0x02

    MAG_I2C_SLAVE_ADDR              = 0x0c
    VALIED_MAG_ID                   = 0x09

    MAG_REG_WIA2                    = 0x01
    MAG_REG_ST1                     = 0x10
    MAG_REG_CNTL2                   = 0x31
    MAG_MODE_CONT_10HZ              = 0b00000010
    READ_MAG_REG_CNT_SENS           = 9

    def __init__(self, cs_ch, freq_hz):
        """ICM-20948制御モジュール初期化

        Args:
            cs_ch (int): CS信号のチャネル番号
            freq_hz (float): SPIクロック周波数[Hz]
        """
        self.spi = SpiController()
        self.spi.configure('ftdi://::/1')
        mode = 0
        self.slave = self.spi.get_port(cs=cs_ch, freq=freq_hz, mode=mode)
        self.__gyro_sample_rate_sec = 1 / self.GYRO_SMPLRT_HZ_DEF
        self.__gyro_fsr_dps = 250
        self.__gyro_x_calib = 0
        self.__gyro_y_calib = 0
        self.__gyro_z_calib = 0
        self.__accl_sample_rate_sec = 1 / self.ACCL_SMPLRT_HZ_DEF
        self.__accl_fsr_g = 0
        self.__conv_regval_to_accl_fsr(self.ACCL_FSR_DEF)
        self.__accl_x_calib = 0
        self.__accl_y_calib = 0
        self.__accl_z_calib = 0

    def __del__(self):
        """デストラクタ
        """
        self.spi.close()

    def __to_signed_int16(self, val):
        """符号付16ビットへ変換

        Args:
            val (int): 変換前の値

        Returns:
            int: 変換後の値
        """
        if val > 32767:
            val -= 65536
        return val
    
    def __sub_int16_ceiling(self, val1, val2):
        """天井付き符号付16ビット変数減算

        Args:
            val1 (int): 減算対象の値
            val2 (int): 減算値

        Returns:
            int: 減算結果
        """
        sub_val = val1 - val2
        if sub_val > 32767:
            sub_val = 32767
        elif sub_val < -32768:
            sub_val = -32768
        return sub_val

    def __conv_regval_to_accl_fsr(self, regval):
        """加速度センサのFSRレジスタ値を実際の値に変換

        Args:
            val (int): Full scale range設定（ACCL_FSR_***を指定する）
        """
        if regval == self.ACCL_FSR_16G:
            self.__accl_fsr_g = 16
        elif regval == self.ACCL_FSR_8G:
            self.__accl_fsr_g = 8
        elif regval == self.ACCL_FSR_4G:
            self.__accl_fsr_g = 4
        elif regval == self.ACCL_FSR_2G:
            self.__accl_fsr_g = 2

    def __spi_xfer(self, xfer_list, read_len=0):
        """SPI転送

        Args:
            xfer_list (_type_): 送信データリスト（リストは1バイトずつ格納する）
            read_len (int): 読み出しデータ数

        Returns:
            bytearray: 読み出しデータ（マスタ送信中の読み出しデータは含まない）
        """
        rdata = self.slave.exchange(xfer_list, read_len)
        return rdata

    def write_reg(self, reg_addr, val_list):
        """レジスタ書き込み

        Args:
            reg_addr (int): レジスタアドレス
            val_list (list): 書き込み値（リスト形式、8ビット値で指定）
        """
        w_data = [[reg_addr], val_list]
        w_data_list = list(itertools.chain.from_iterable(w_data))
        self.__spi_xfer(w_data_list)

    def write_reg_1bit(self, reg_addr, bit, is_set_1):
        """レジスタ1ビット書き込み

        Args:
            reg_addr (int): レジスタアドレス
            bit (int): 書き込みビット
            is_set_1 (bool): 書き込み値（True=1、False=0）
        """
        rdata = self.read_reg_1(reg_addr)
        wbit = 0x01 << bit
        if is_set_1:
            rdata |= wbit
        else:
            rdata &= ~wbit
        self.write_reg(reg_addr, [rdata])

    def read_reg(self, reg_addr, read_len):
        """レジスタ読み出し

        Args:
            reg_addr (int): レジスタアドレス
            read_len (int): 読み出しバイト数

        Returns:
            list: 読み出し値
        """
        rdata = self.__spi_xfer([reg_addr | 0x80], read_len)
        rdata_list = bytearray(rdata)
        return rdata_list

    def read_reg_1(self, reg_addr):
        """1レジスタ読み出し

        Args:
            reg_addr (int): レジスタアドレス

        Returns:
            int: 読み出し値
        """
        return self.read_reg(reg_addr, 1)[0]

    def set_bank(self, bank):
        """バンク切り替え

        Args:
            bank (int): バンク番号

        Returns:
            bool: 処理が成功したか否か
        """
        if bank > 3:
            return False
        bank = ((bank << 4) & 0x30)
        self.write_reg(self.REG_BANK_SEL, [bank])
        return True

    def check_id(self):
        """ID読み出し

        Returns:
            bool: IDを正しく読み出せたか否か
        """
        self.set_bank(0)
        rdata = self.read_reg(self.AGB0_REG_WHO_AM_I, 1)
        is_valid = (rdata[0] == self.VALIED_CHIP_ID)
        return is_valid

    def soft_reset(self):
        """ソフトリセット実行
        """
        self.set_bank(0)
        self.write_reg_1bit(self.AGB0_REG_PWR_MGMT_1, 7, True)
        time.sleep(0.05)

    def sleep_mode(self, is_on):
        """スリープモード変更

        Args:
            is_on (bool): スリープモードにするか否か
        """
        self.set_bank(0)
        self.write_reg_1bit(self.AGB0_REG_PWR_MGMT_1, 6, is_on)

    def fifo_en(self, is_en):
        """FIFO有効/無効設定

        Args:
            is_en (bool): FIFO有効/無効
        """
        self.set_bank(0)
        self.write_reg_1bit(self.AGB0_REG_USER_CTRL, 6, is_en)

    def reset_fifo(self):
        """FIFOリセット
        """
        self.set_bank(0)
        self.write_reg(self.AGB0_REG_INT_ENABLE, [0x00])    # Disable all interrupts
        self.write_reg(self.AGB0_REG_INT_ENABLE_1, [0x00])
        self.write_reg(self.AGB0_REG_INT_ENABLE_2, [0x00])
        self.write_reg(self.AGB0_REG_INT_ENABLE_3, [0x00])
        self.write_reg(self.AGB0_REG_FIFO_EN_1, [0x00])     # Disable FIFO
        self.write_reg(self.AGB0_REG_FIFO_EN_2, [0x00])
        self.fifo_en(False)
        self.write_reg(self.AGB0_REG_FIFO_RST, [0x1f])      # FIFO reset
        time.sleep(0.01)
        self.write_reg(self.AGB0_REG_FIFO_RST, [0x00])

    def set_fifo_sens(self, enable_sens, is_en):
        """FIFOに格納するセンサデータの設定

        Args:
            enable_sens (int): FIFOに格納するセンサデータ（FIFO_EN_***をORで設定）
            is_en (bool): FIFOを有効にするか否か
        """
        self.set_bank(0)
        self.write_reg(self.AGB0_REG_FIFO_EN_2, [enable_sens])
        if is_en:
            self.fifo_en(True)

    def set_fifo_intr(self, is_overflow, is_wm):
        """FIFO関連割り込み設定

        Args:
            is_overflow (bool): オーバーフローで割り込みを発生させるか否か
            is_wm (bool): ウォーターマークで割り込みを発生させるか否か
        """
        self.set_bank(0)
        self.write_reg_1bit(self.AGB0_REG_INT_ENABLE_2, 0, is_overflow)
        self.write_reg_1bit(self.AGB0_REG_INT_ENABLE_3, 0, is_wm)

    def get_fifo_intr(self, is_check_overflow, is_check_wm):
        """FIFO関連割り込み状態取得

        Args:
            is_check_overflow (bool): オーバーフローをチェックするか否か（4096バイトでTrue）
            is_check_wm (bool): ウォーターマークをチェックするか否か（2048バイトでTrue）

        Returns:
            bool: FIFO関連割り込みの有無
        """
        is_overflow = (self.read_reg_1(self.AGB0_REG_INT_STATUS_2) != 0x00)
        is_wm = (self.read_reg_1(self.AGB0_REG_INT_STATUS_3) != 0x00)
        return (is_overflow & is_check_overflow) or (is_wm & is_check_wm)

    def get_fifo_count(self):
        """FIFO格納データ数取得

        Returns:
            int: FIFO格納データ数
        """
        rdata = self.read_reg(self.AGB0_REG_FIFO_COUNT_H, 2)
        return rdata[0] << 8 | rdata[1]

    def read_fifo(self, count):
        """FIFO読み出し

        Args:
            count (int): FIFO読み出しデータ数

        Returns:
            list: FIFO読み出しデータ
        """
        return self.read_reg(self.AGB0_REG_FIFO_R_W, count)
    
    def decode_fifo_data(self, data, enable_sens, is_en_calib_accl=True, is_en_calib_gyro=True):
        """FIFO読み出しデータのデコード

        Args:
            data (list): FIFO読み出しデータ
            enable_sens (int): FIFOに格納するセンサデータ（FIFO_EN_***をORで設定）
            is_en_calib_accl (bool): キャリブレーションによる補正を行うか否か（加速度センサ）
            is_en_calib_gyro (bool): キャリブレーションによる補正を行うか否か（ジャイロセンサ）

        Returns:
            np.ndarray(np.int16): 加速度、ジャイロX軸、ジャイロY軸、ジャイロZ軸のRawデータ

        Remarks:
            加速度の戻り値は、配列形式でX, Y, Z, X, Y, ..., Zの順に並んでいる。
        """
        accl_raw = None
        gyro_x_raw = None
        gyro_y_raw = None
        gyro_z_raw = None
        is_en_accl = enable_sens & self.FIFO_EN_ACCL
        is_en_gyro_z = enable_sens & self.FIFO_EN_GYRO_Z
        is_en_gyro_y = enable_sens & self.FIFO_EN_GYRO_Y
        is_en_gyro_x = enable_sens & self.FIFO_EN_GYRO_X
        packet_size = 0
        if is_en_accl:
            packet_size += 6
        if is_en_gyro_z:
            packet_size += 2
        if is_en_gyro_y:
            packet_size += 2
        if is_en_gyro_x:
            packet_size += 2
        if packet_size == 0:
            return accl_raw, gyro_x_raw, gyro_y_raw, gyro_z_raw
        packet_count = int(len(data) / packet_size)
        
        if is_en_accl:
            accl_raw = np.empty(packet_count * 3, dtype=np.int16)
        if is_en_gyro_z:
            gyro_z_raw = np.empty(packet_count, dtype=np.int16)
        if is_en_gyro_y:
            gyro_y_raw = np.empty(packet_count, dtype=np.int16)
        if is_en_gyro_x:
            gyro_x_raw = np.empty(packet_count, dtype=np.int16)
        
        data_num = 0
        
        accl_calib_x = self.__accl_x_calib if is_en_calib_accl else 0
        accl_calib_y = self.__accl_y_calib if is_en_calib_accl else 0
        accl_calib_z = self.__accl_z_calib if is_en_calib_accl else 0
        gyro_calib_x = self.__gyro_x_calib if is_en_calib_gyro else 0
        gyro_calib_y = self.__gyro_y_calib if is_en_calib_gyro else 0
        gyro_calib_z = self.__gyro_z_calib if is_en_calib_gyro else 0
        for i in range(packet_count):
            if is_en_accl:
                accl_raw[i * 3 + 0] = self.__to_signed_int16((data[data_num + 0] << 8) | (data[data_num + 1] & 0xff))
                accl_raw[i * 3 + 0] = self.__sub_int16_ceiling(accl_raw[i * 3 + 0], accl_calib_x)
                accl_raw[i * 3 + 1] = self.__to_signed_int16((data[data_num + 2] << 8) | (data[data_num + 3] & 0xff))
                accl_raw[i * 3 + 1] = self.__sub_int16_ceiling(accl_raw[i * 3 + 1], accl_calib_y)
                accl_raw[i * 3 + 2] = self.__to_signed_int16((data[data_num + 4] << 8) | (data[data_num + 5] & 0xff))
                accl_raw[i * 3 + 2] = self.__sub_int16_ceiling(accl_raw[i * 3 + 2], accl_calib_z)
                data_num += 6
            if is_en_gyro_x:
                gyro_x_raw[i] = self.__to_signed_int16((data[data_num] << 8) | (data[data_num + 1] & 0xff))
                gyro_x_raw[i] = self.__sub_int16_ceiling(gyro_x_raw[i], gyro_calib_x)
                data_num += 2
            if is_en_gyro_y:
                gyro_y_raw[i] = self.__to_signed_int16((data[data_num] << 8) | (data[data_num + 1] & 0xff))
                gyro_y_raw[i] = self.__sub_int16_ceiling(gyro_y_raw[i], gyro_calib_y)
                data_num += 2
            if is_en_gyro_z:
                gyro_z_raw[i] = self.__to_signed_int16((data[data_num] << 8) | (data[data_num + 1] & 0xff))
                gyro_z_raw[i] = self.__sub_int16_ceiling(gyro_z_raw[i], gyro_calib_z)
                data_num += 2
        return accl_raw, gyro_x_raw, gyro_y_raw, gyro_z_raw

    def get_fifo_accl_axis(self, accl_raw_fifo, axis):
        """FIFOから取得した加速度データから特定の軸のデータを取得

        Args:
            accl_raw_fifo (np.ndarray): FIFOから取得した加速度データ
            axis (int): 取り出す軸（AXIS_*）で指定

        Returns:
            np.ndarray: 指定軸のデータ
        """
        return accl_raw_fifo[axis:len(accl_raw_fifo):3]

    def set_fsr_accl(self, val):
        """加速度用Full scale range設定

        Args:
            val (int): Full scale range設定（ACCL_FSR_***を指定する）
        """
        if val > self.ACCL_FSR_MAX:
            return
        self.set_bank(2)
        rdata = self.read_reg_1(self.AGB2_REG_ACCEL_CONFIG_1)
        rdata &= ~(0b00000110)
        rdata |= (val << 1)
        self.write_reg(self.AGB2_REG_ACCEL_CONFIG_1, [rdata])
        self.__conv_regval_to_accl_fsr(val)

    def set_lpf_gyro(self, is_on, val):
        """ジャイロ用LPF周波数設定

        Args:
            is_on (bool): LPFを有効にするか否か
            val (int): 周波数設定（GYRO_DLPFCFG_FC_***を指定する）
        """
        self.set_bank(2)
        rdata = self.read_reg_1(self.AGB2_REG_GYRO_CONFIG_1)
        rdata &= ~(0b00111001)
        if is_on:
            rdata |= ((val << 3) | (0x01 << 0))
        self.write_reg(self.AGB2_REG_GYRO_CONFIG_1, [rdata])

    def set_lpf_accl(self, is_on, val):
        """加速度用LPF周波数設定

        Args:
            is_on (bool): LPFを有効にするか否か
            val (int): 周波数設定（ACCL_DLPFCFG_FC_***を指定する）
        """
        self.set_bank(2)
        rdata = self.read_reg_1(self.AGB2_REG_ACCEL_CONFIG_1)
        rdata &= ~(0b00111001)
        if is_on:
            rdata |= ((val << 3) | (0x01 << 0))
        self.write_reg(self.AGB2_REG_ACCEL_CONFIG_1, [rdata])

    def set_sample_rate_gyro(self, hz):
        """ジャイロセンサのサンプリングレート設定

        Args:
            hz (int): 周波数[Hz]

        Returns:
            float: 実際の周波数
        """
        if hz < self.GYRO_SMPLRT_HZ_MIN:
            hz = self.GYRO_SMPLRT_HZ_MIN
        elif hz > self.GYRO_SMPLRT_HZ_MAX:
            hz = self.GYRO_SMPLRT_HZ_MAX
        div = int(self.GYRO_SMPLRT_HZ_MAX / hz - 1)
        act_hz = 1125 / (1 + div)
        self.__gyro_sample_rate_sec = 1 / act_hz
        self.set_bank(2)
        self.write_reg(self.AGB2_REG_GYRO_SMPLRT_DIV, [div])
        return act_hz

    def set_sample_rate_accl(self, hz):
        """加速度センサのサンプリングレート設定

        Args:
            hz (int): 周波数[Hz]
        
        Returns:
            float: 実際の周波数
        """
        if hz < self.ACCL_SMPLRT_HZ_MIN:
            hz = self.ACCL_SMPLRT_HZ_MIN
        elif hz > self.ACCL_SMPLRT_HZ_MAX:
            hz = self.ACCL_SMPLRT_HZ_MAX
        div = int(self.ACCL_SMPLRT_HZ_MAX / hz - 1)
        act_hz = 1125 / (1 + div)
        self.__accl_sample_rate_sec = 1 / act_hz
        self.set_bank(2)
        self.write_reg(self.AGB2_REG_ACCEL_SMPLRT_DIV_1, [div >> 8, div & 0xff])
        return act_hz

    def exec_calib_gyro(self, calc_sample_count):
        """ジャイロセンサのキャリブレーション値取得

        Args:
            calc_sample_count (int): キャリブレーションに必要な各軸ごとのサンプル数

        Returns:
            float, float, float: X/Y/Z軸のキャリブレーション値

        Remarks:
            Rawデータからキャリブレーション値を減算する。
            キャリブレーション時は静止状態で実施する。
        """
        self.reset_fifo()
        fifo_enable_sens = self.FIFO_EN_GYRO_X | self.FIFO_EN_GYRO_Y | self.FIFO_EN_GYRO_Z
        self.set_fifo_sens(fifo_enable_sens, True)
        get_sample_count = 0
        gyro_x_total = 0
        gyro_y_total = 0
        gyro_z_total = 0
        while True:
            fifo_count = self.get_fifo_count()
            _, gyro_x_raw, gyro_y_raw, gyro_z_raw = self.decode_fifo_data(self.read_fifo(fifo_count), fifo_enable_sens)
            gyro_x_total += np.sum(gyro_x_raw)
            gyro_y_total += np.sum(gyro_y_raw)
            gyro_z_total += np.sum(gyro_z_raw)
            get_sample_count += int(fifo_count / 6)
            if get_sample_count >= calc_sample_count:
                break
            time.sleep(0.1)
        self.__gyro_x_calib = gyro_x_total / get_sample_count
        self.__gyro_y_calib = gyro_y_total / get_sample_count
        self.__gyro_z_calib = gyro_z_total / get_sample_count
        return self.__gyro_x_calib, self.__gyro_y_calib, self.__gyro_z_calib

    def exec_calib_accl(self, calc_sample_count):
        """加速度センサのキャリブレーション値取得

        Args:
            calc_sample_count (int): キャリブレーションに必要な各軸ごとのサンプル数

        Returns:
            float, float, float: X/Y/Z軸のキャリブレーション値

        Remarks:
            Rawデータからキャリブレーション値を減算する。
            キャリブレーション時は水平状態で実施する。
        """
        self.reset_fifo()
        fifo_enable_sens = self.FIFO_EN_ACCL
        self.set_fifo_sens(fifo_enable_sens, True)
        get_sample_count = 0
        accl_x_total = 0
        accl_y_total = 0
        accl_z_total = 0
        while True:
            fifo_count = self.get_fifo_count()
            accl_raw, _, _, _ = self.decode_fifo_data(self.read_fifo(fifo_count), fifo_enable_sens)
            data_count = len(accl_raw)
            sample_count = int(data_count / 3)
            accl_x_total += np.sum(accl_raw[0:data_count:3])
            accl_y_total += np.sum(accl_raw[1:data_count:3])
            accl_z_total += np.sum(accl_raw[2:data_count:3])
            get_sample_count += sample_count
            if get_sample_count >= calc_sample_count:
                break
            time.sleep(0.02)
        self.__accl_x_calib = accl_x_total / get_sample_count
        self.__accl_y_calib = accl_y_total / get_sample_count
        self.__accl_z_calib = accl_z_total / get_sample_count
        return self.__accl_x_calib, self.__accl_y_calib, self.__accl_z_calib

    def get_raw_gyro(self):
        """ジャイロセンサRawデータ読み出し（レジスタ）

        Returns:
            int, int, int: X/Y/Z軸のRawデータ（キャリブレーションによる補正未実施）
        """
        self.set_bank(0)
        rdata = self.read_reg(self.AGB0_REG_GYRO_XOUT_H, 6)
        gyro_x = ((rdata[0] << 8) | (rdata[1] & 0xff))
        gyro_y = ((rdata[2] << 8) | (rdata[3] & 0xff))
        gyro_z = ((rdata[4] << 8) | (rdata[5] & 0xff))
        gyro_x = self.__to_signed_int16(gyro_x)
        gyro_y = self.__to_signed_int16(gyro_y)
        gyro_z = self.__to_signed_int16(gyro_z)
        return gyro_x, gyro_y, gyro_z

    def get_raw_accl(self):
        """加速度センサRawデータ読み出し（レジスタ）

        Returns:
            int, int, int: X/Y/Z軸のRawデータ（キャリブレーションによる補正未実施）
        """
        self.set_bank(0)
        rdata = self.read_reg(self.AGB0_REG_ACCEL_XOUT_H, 6)
        accl_x = ((rdata[0] << 8) | (rdata[1] & 0xff))
        accl_y = ((rdata[2] << 8) | (rdata[3] & 0xff))
        accl_z = ((rdata[4] << 8) | (rdata[5] & 0xff))
        accl_x = self.__to_signed_int16(accl_x)
        accl_y = self.__to_signed_int16(accl_y)
        accl_z = self.__to_signed_int16(accl_z)
        return accl_x, accl_y, accl_z

    def calc_accl_velocity(self, raw):
        """加速度センサによる速度算出（長方形近似で積分）

        Args:
            raw (np.ndarray): 各軸のRawデータ

        Returns:
            int: 入力データにおける速度[m/s]
        """
        return np.sum(raw / 32768 * self.__accl_fsr_g * 9.81) * self.__accl_sample_rate_sec

    def calc_gyro_degree(self, raw):
        """変化した角度取得（長方形近似で積分）

        Args:
            raw (np.ndarray): 各軸のRawデータ

        Returns:
            int: 当該軸の角度変化[deg]
        """
        return np.sum(self.__gyro_fsr_dps * (raw / 32768) * self.__gyro_sample_rate_sec)

    def i2c_mst_pass_through(self, is_en):
        """内部I2Cマスタパススルーモード設定

        Args:
            is_en (bool): 有効/無効設定
        """
        self.set_bank(0)
        self.write_reg_1bit(self.AGB0_REG_INT_PIN_CONFIG, 1, is_en)

    def i2c_mst_enable(self, is_en):
        """内部I2Cマスタ設定

        Args:
            is_en (bool): 有効/無効設定
        """
        self.i2c_mst_pass_through(False)
        self.set_bank(3)
        # 345.6kHz/Duty 46.67%(Recommended), I2C stop between slave reads
        self.write_reg(self.AGB3_REG_I2C_MST_CTRL, [0b00010111])
        self.set_bank(0)
        self.write_reg_1bit(self.AGB0_REG_USER_CTRL, 5, is_en)

    def i2c_mst_slv_en(self, ch, slv_addr, reg, is_read, len, write_val):
        """内部I2Cスレーブ通信チャネル有効化

        Args:
            ch (int): チャネル番号（0～3）
            slv_addr (int): スレーブアドレス
            reg (int): レジスタアドレス
            is_read (bool): True=Read、False=Write
            len (int): Read/Writeデータ長
            write_val (int): 書き込みデータ（1バイト、読み出し時は不問）
        """
        if ch > 3:
            return
        slv_addr_reg = self.AGB3_REG_I2C_SLV0_ADDR + 4 * ch
        slv_reg_reg = self.AGB3_REG_I2C_SLV0_REG + 4 * ch
        slv_ctrl_reg = self.AGB3_REG_I2C_SLV0_CTRL + 4 * ch
        slv_do_reg = self.AGB3_REG_I2C_SLV0_DO + 4 * ch
        self.set_bank(3)
        if is_read:
            if len > 15:
                return
            slv_addr |= 0x80
        else:
            self.write_reg(slv_do_reg, [write_val])
        self.write_reg(slv_addr_reg, [slv_addr])
        self.write_reg(slv_reg_reg, [reg])
        self.write_reg(slv_ctrl_reg, [0b10000000 | len])

    def i2c_mst_slv_dis(self, ch):
        """内部I2Cスレーブ通信チャネル無効化

        Args:
            ch (int): チャネル番号（0～3）
        """
        if ch > 3:
            return
        slv_ctrl_reg = self.AGB3_REG_I2C_SLV0_CTRL + 4 * ch
        self.set_bank(3)
        self.write_reg(slv_ctrl_reg, [0x00])

    def i2c_slv4_txn(self, slv_addr, is_read, reg, val=0x00):
        """内部I2Cスレーブ通信チャネル4での通信

        Args:
            slv_addr (int): スレーブアドレス
            is_read (bool): True=Read、False=Write
            reg (int): レジスタアドレス
            val (hexadecimal, optional): 書き込みデータ（1バイト、読み出し時は不問）

        Returns:
            int: 読み出し値（書き込み時は0を返す）
        """
        if is_read:
            slv_addr |= 0x80
        self.set_bank(3)
        self.write_reg(self.AGB3_REG_I2C_SLV4_ADDR, [slv_addr])
        self.write_reg(self.AGB3_REG_I2C_SLV4_REG, [reg])
        if not is_read:
            self.write_reg(self.AGB3_REG_I2C_SLV4_DO, [val])
        self.write_reg(self.AGB3_REG_I2C_SLV4_CTRL, [0b10000001])

        slave_access_tmo = False
        slave_access_time = 0
        slave4_done = 0
        is_success = True
        while not slave_access_tmo:
            self.set_bank(0)
            slave4_done = self.read_reg_1(self.AGB0_REG_I2C_MST_STATUS)
            if (slave4_done & (0x01 << 6)):
                break
            else:
                if slave_access_time >= 1:
                    slave_access_tmo = True
                slave_access_time += 0.01
                time.sleep(0.01)
        if slave_access_tmo:
            is_success = False
        if (slave4_done & (0x01 << 4)):
            is_success = False
        
        self.write_reg(self.AGB3_REG_I2C_SLV4_CTRL, [0x00])
        if not is_success:
            return -1

        self.set_bank(3)
        ret_val = 0
        if is_read:
            ret_val = self.read_reg_1(self.AGB3_REG_I2C_SLV4_DI)
        return ret_val

    def check_id_mag(self, tmo_sec=0.5):
        """磁気センサID読み出し

        Args:
            tmo_sec (float, optional): 読み出しタイムアウト時間[秒]

        Returns:
            bool: 正しく読み出せたか否か
        """
        self.i2c_mst_slv_en(self.I2C_MAG_SLV_CH, self.MAG_I2C_SLAVE_ADDR, self.MAG_REG_WIA2, True, 1, 0)
        self.set_bank(0)
        read_time = 0
        id = 0
        while read_time < tmo_sec:
            time.sleep(0.01)
            id = self.read_reg_1(self.AGB0_REG_EXT_SLV_SENS_DATA_00)
            if id == self.VALIED_MAG_ID:
                break
            read_time += 0.01
        self.i2c_mst_slv_dis(self.I2C_MAG_SLV_CH)
        return (id == self.VALIED_MAG_ID)

    def get_mag_enable(self, mode):
        """磁気センサ読み出し有効化

        Args:
            mode (int): 読み出しモード（MAG_MODE_***の値を設定する）
        """
        self.i2c_mst_slv_en(self.I2C_MAG_SLV_CH, self.MAG_I2C_SLAVE_ADDR, self.MAG_REG_CNTL2, False, 1, mode)
        time.sleep(0.1)
        self.i2c_mst_slv_dis(self.I2C_MAG_SLV_CH)
        self.i2c_mst_slv_en(self.I2C_MAG_SLV_CH, self.MAG_I2C_SLAVE_ADDR, self.MAG_REG_ST1, True, self.READ_MAG_REG_CNT_SENS, 0)
        time.sleep(0.1)

    def get_mag_disable(self):
        """磁気センサ読み出し無効化
        """
        self.i2c_mst_slv_dis(self.I2C_MAG_SLV_CH)

    def get_raw_mag(self, is_check_err=False):
        """磁気センサRawデータ読み出し（レジスタ）

        Args:
            is_check_err (bool): エラーチェックするか否か（エラー時に文字出力する）

        Returns:
            int, int, int: X/Y/Z軸のRawデータ
        """
        self.set_bank(0)
        rdata = self.read_reg(self.AGB0_REG_EXT_SLV_SENS_DATA_00, self.READ_MAG_REG_CNT_SENS)
        mag_x = ((rdata[2] << 8) | (rdata[1] & 0xff))
        mag_y = ((rdata[4] << 8) | (rdata[3] & 0xff))
        mag_z = ((rdata[6] << 8) | (rdata[5] & 0xff))
        mag_x = self.__to_signed_int16(mag_x)
        mag_y = self.__to_signed_int16(mag_y)
        mag_z = self.__to_signed_int16(mag_z)
        if is_check_err:
            if ((rdata[8] & 0x80) == 0x80):
                print("Overflow occurred.")
            if rdata[0] != 0x00:
                print(f"Read error (ST1:{hex(rdata[0])})")
        return mag_x, mag_y, mag_z
